#!/usr/bin/env python3

import sys
import hashlib
import os
import argparse

BUF_SIZE = 65536

def find_dupes(file_directory, delete_flag, recursive_flag):
    hash_arr = []
    dupe_files = []
    nested_dirs = []

    md5 = hashlib.md5()
    file_list = os.scandir(file_directory)
    for file in file_list:
        valid_file = not (file.is_dir() or file.is_symlink()) and file.is_file()
        if valid_file:
            with open(file.path, "rb") as file_obj:
                data = bytes()
                while data:
                    data = file_obj.read(BUF_SIZE)
                    md5.update(data)
            try:
                digest = md5.hexdigest()
                if digest not in hash_arr:
                    hash_arr.append(digest)
                else:
                    dupe_files.append(file.path)
            except Exception as e:
                print(e)
                sys.exit(1)
        else:
            if file.is_dir():
                nested_dirs.append(file.path)
    
    if delete_flag:
        for file in dupe_files:
            os.remove(file)
    
    if recursive_flag:
        for directory in nested_dirs:
            temp = find_dupes(directory, delete_flag, recursive_flag)
            dupe_files += temp

    return dupe_files

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Find files with identical contents in a given directory")
    parser.add_argument("directory", help="Directory to search")
    parser.add_argument("-r", action='store_true', help="Recursively search directory")
    parser.add_argument("-d", action='store_true', help="Delete all found duplicates")
    parser.add_argument("-q", action='store_true', help="Suppress output")

    args = parser.parse_args()
    directory_to_clean = args.directory
    recursive = args.r
    delete = args.d
    quiet = args.q
    duplicate_files = find_dupes(directory_to_clean, delete, recursive)
    if duplicate_files:
        if not quiet:
            for file in duplicate_files:
                print(file)
        sys.exit(1)
    else:
        sys.exit(0)